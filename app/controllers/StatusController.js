var statusController = {};

statusController.status = function(req, res) {
    res.send("Application server running : "+new Date());
};

module.exports = statusController;