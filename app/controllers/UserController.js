var mongoose = require("mongoose");
var User = require("../models/User");

var userController = {};


userController.createUser = function(req, res) {
    var newUser = new User(req.body);
    newUser.save(function(err, result) {
      if (err)
        res.send(err);
        res.json(result);
    });
};


userController.getUsers = function(req, res) {
    console.info("getUsers....")
    User.find({},{__v:false,createdTimestamp:false}, function(err,result) {
      if (err)
        res.send(err);
        res.json(result);
    });
  };

  userController.authenticateUser = function(req, res) {
    console.info("authenticateUser....")
    User.find({username:req.body.username,password:req.body.password},{__v:false,createdTimestamp:false}, function(err, result) {
      if (err)
        res.send(err);
        res.json(result);
    });
  };

  userController.addUserTrip = function(req, res) {
    var jsonString = JSON.stringify(req.body.trip);
    var jsonData = JSON.parse(jsonString);
    jsonData.departure.trip_date = new Date(jsonData.departure.trip_date);
    jsonData.departure.created_date = new Date();
    jsonData.arrival.trip_date = new Date(jsonData.arrival.trip_date);
    jsonData.arrival.created_date = new Date();
    //console.info(jsonData);
    console.info("addUserTrip....");
    User.update({_id: req.body.id}, { $push: { trips: jsonData}}, function (err, result) {
      if (err)
        res.send(err);
        res.json(result);
    });
  };

module.exports = userController;
