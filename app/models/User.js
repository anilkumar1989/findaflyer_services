var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
    username: String,
    password: String,
    email: String,
    ph1: String,
    ph2: String,
    identity: String,
    identity_path: String,
    identity_expiry : { type: Date},
    status: { type: String, default: 'pending' },
    active: { type: String, default: 'Y' },
    createdTimestamp: { type: Date, default: Date.now },
    updatedTimestamp: { type: Date},
    login_type: String,
    trips: { type: Array},
    requests: Array
  });
  
  module.exports = mongoose.model('User', UserSchema);