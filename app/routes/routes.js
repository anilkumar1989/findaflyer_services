module.exports = function(app) {
    var userController = require('../controllers/UserController');
    var statusController = require('../controllers/StatusController');
  
    // user routes
    app.route('/users/createUser').post(userController.createUser);
    app.route('/users/getUsers').get(userController.getUsers);
    app.route('/users/authenticateUser').post(userController.authenticateUser);
    app.route('/users/addUserTrip').post(userController.addUserTrip);
    

    // status routes
    app.route('/status').get(statusController.status);
  
  };