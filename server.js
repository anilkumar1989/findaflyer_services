var express = require("express");
var app = express();
var cors = require("cors");
var http = require("http");
var url = require("url");
var fs = require("fs");
var bodyParser = require("body-parser");
var config = require('./config/database.config.js');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect(config.url, {
  useMongoClient: true
});
mongoose.connection.on('error', function() {
  console.log('Could not connect to the database.');
  process.exit();
});
mongoose.connection.once('open', function() {
  console.log("Successfully connected to the database");
})

/*mongoose.connect(config.url)
  .then(() =>  console.log('db connection succesful'))
  .catch((err) => console.error(err));*/



var routes = require('./app/routes/routes');
routes(app);


var server = app.listen(8686, function(){
  var host = server.address().address
  var port = server.address().port;
  console.log("host -----> "+host);
  console.log("app listening at http://%s:%s", host, port)
})


module.exports = app;
